//= require Control
//= require TrafficLight
//= require StreetSection

describe("Control object", function() {

	var mapElement  = document.createElement('div');
    var mapLocation = {lat: -23.558147, lng: -46.660198};
    var mapZoom = 15;
    var map = new StreetMap(mapLocation, mapElement, mapZoom);
    var color1 = "#FFFFFF";
	var color2 = "#000000";
	var path = [{lat:-10, lng: -10}, {lat:-10, lng: -11}];
	var s1 = new StreetSection(path, color1, color2, map);
	var s2 = new StreetSection(path, color1, color2, map);
	var s3 = new StreetSection(path, color1, color2, map);
	var t1 = new TrafficLight(mapLocation, mapZoom, map);
	var t2 = new TrafficLight(mapLocation, mapZoom, map);
	var t3 = new TrafficLight(mapLocation, mapZoom, map);
	var control = new Control(map);

    it("should belong to a map", function() {
    	expect(control.map).toBe(map);
    });

    it("should be able to make traffic lights disappear", function() {
    	control.setVisibilityInAll([t1, t2, t3], false);
    	expect(t1.light.visible).toBeFalsy();
    	expect(t2.light.visible).toBeFalsy();
    	expect(t3.light.visible).toBeFalsy();
    });

    it("should be able to make street sections disappear", function() {
    	control.setVisibilityInAll([s1, s2, s3], false);
    	expect(s1.section.visible).toBeFalsy();
    	expect(s2.section.visible).toBeFalsy();
    	expect(s3.section.visible).toBeFalsy();
    });

    it("should be able to make traffic lights visible again", function() {
    	control.setVisibilityInAll([t1, t2, t3], true);
    	expect(t1.light.visible).toBeTruthy();
    	expect(t2.light.visible).toBeTruthy();
    	expect(t3.light.visible).toBeTruthy();
    });

    it("should be able to make street sections visible again", function() {
    	control.setVisibilityInAll([s1, s2, s3], true);
    	expect(s1.section.visible).toBeTruthy();
    	expect(s2.section.visible).toBeTruthy();
    	expect(s3.section.visible).toBeTruthy();
    });

    var element = document.createElement('input');
	element.setAttribute("type", "checkbox"); 

	it("should turn traffic lights invisible if this option is checked", function() {
		element.checked = false;
    	control.toggleVisibility(element, [t1, t2, t3]);
    	expect(t1.light.visible).toBeFalsy();
    	expect(t2.light.visible).toBeFalsy();
    	expect(t3.light.visible).toBeFalsy();
    });

    it("should turn street sections invisible if this option is checked", function() {
		element.checked = false;
    	control.toggleVisibility(element, [s1, s2, s3]);
    	expect(s1.section.visible).toBeFalsy();
    	expect(s2.section.visible).toBeFalsy();
    	expect(s3.section.visible).toBeFalsy();
    });

    it("should turn traffic lights visible if this option is checked", function() {
		element.checked = true;
    	control.toggleVisibility(element, [t1, t2, t3]);
    	expect(t1.light.visible).toBeTruthy();
    	expect(t2.light.visible).toBeTruthy();
    	expect(t3.light.visible).toBeTruthy();
    });

    it("should turn street sections visible if this option is checked", function() {
		element.checked = true;
    	control.toggleVisibility(element, [s1, s2, s3]);
    	expect(s1.section.visible).toBeTruthy();
    	expect(s2.section.visible).toBeTruthy();
    	expect(s3.section.visible).toBeTruthy();
    });

    it("should turn street sections visible or not if a user want it to", function() {
    	control.dataCheckbox = false;
    	var func = control.dataClicked();
    	func();
    	expect(control.dataCheckbox).toBeTruthy();
    	func();
    	expect(control.dataCheckbox).toBeFalsy();
    });

    it("should turn traffic lights visible or not if a user want it to", function() {
    	control.lightsCheckbox = false;
    	var func = control.lightsClicked();
    	func();
    	expect(control.lightsCheckbox).toBeTruthy();
    	func();
    	expect(control.lightsCheckbox).toBeFalsy();
    });

    map.drawStreetSections();
    var sections = map.streetSections;

    it("should display traffic speed data if a user want it to", function() {
    	var func = control.infoSpeedClicked();
    	func();    	
    	for (i in sections)
    		expect(sections[i].section.strokeColor).toEqual(sections[i].speedColor);
    });

    it("should display traffic volume data if a user want it to", function() {
    	var func = control.infoVolumeClicked();
    	func();
    	for (i in sections)
			expect(sections[i].section.strokeColor).toEqual(sections[i].volumeColor);
    });
});