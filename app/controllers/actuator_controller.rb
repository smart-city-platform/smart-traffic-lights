class ActuatorController < ApplicationController
    def show 
        @actuator = Actuator.find(params[:id])
        render :json => @actuator
    end
end
