class Actuator < ActiveRecord::Base
    has_many :traffic_lights
    has_many :sensors, through: :actuator_sensors
end
