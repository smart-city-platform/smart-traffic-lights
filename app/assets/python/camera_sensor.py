import cv2
from cv2 import cv
import numpy as np

from os import system
import time
import json

class CameraSensor:
  def __init__(self, src = '../videos/traffic.avi', disp = True):
    self.backsub = cv2.BackgroundSubtractorMOG()
    self.capture = cv2.VideoCapture(src)
    self.kernel1 = np.ones((2,2),np.uint8)
    self.kernel2 = np.ones((5,5),np.uint8)
    self.disp = disp
    self.result = {}
    self.ret, self.frame = self.capture.read()
    # params for corner detection
    self.feature_params = dict( maxCorners = 100,
                   qualityLevel = 0.3,
                   minDistance = 7,
                   blockSize = 7 )
    # LK params
    self.lk_params = dict( winSize  = (15,15),
                   maxLevel = 2,
                   criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT,
                   10, 0.03))
    self.speed_factor = .6

  def counter(self):
    self.cont = 0
    
    for self.contour, self.hier in zip(self.contours, self.hierarchy):
      (x,y,w,h) = cv2.boundingRect(self.contour)
      
      if w > 10 and h > 10:
        self.cont += 1
        cv2.rectangle(self.frame, (x,y), (x+w,y+h), (255, 0, 0), 2)
        cv2.putText(self.frame, str(self.cont), (x,y-5), cv2.FONT_HERSHEY_SIMPLEX,
                    0.5, (255, 0, 0), 2)

  def speed(self):
    # calculate optical flow
    self.p1, st, err = cv2.calcOpticalFlowPyrLK(self.previous, cv2.cvtColor(self.frame, cv2.COLOR_BGR2GRAY), self.p0, None, **self.lk_params)
    self.sp = np.sqrt(np.square(self.p1 - self.p0).sum())
    
  def get_speed(self):
      return self.sp*self.speed_factor
      
  def run(self):
    self.running = True
    if self.capture:
      self.process()

  def process(self):
    while self.running:
      self.read()
      if self.ret:
        self.filtering()
        self.counter()
        self.speed()
        self.cont_tofile()
        if self.disp:
            self.display()
      if cv2.waitKey(100) == 27: # 27 is the code for ESC
        self.stop()

  def get_cont(self):
    return self.cont

  def cont_tofile(self):
    self.result["cont"] = str(self.get_cont())
    self.result["speed"] = str(self.get_speed())
    with open('output.json', 'w') as fp:
        json.dump(self.result, fp)
    #fp.close()

  def stop(self):
    self.running = False

  def read(self):
    self.previous = cv2.cvtColor(self.frame, cv2.COLOR_BGR2GRAY)
    self.previousRGB = self.frame
    self.p0 = cv2.goodFeaturesToTrack(self.previous, mask = None, **self.feature_params)
    self.ret, self.frame = self.capture.read()
    if self.frame is None: # restart reading frames
      self.capture.set(cv2.cv.CV_CAP_PROP_POS_FRAMES, 0)
      self.frame = self.previousRGB

  def display(self):
    cv2.imshow("Track", self.frame)
    cv2.imshow("background sub", self.fgmask)
    #time.sleep(.1)

  def filtering(self):
    self.fgmask = cv2.erode(self.backsub.apply(self.frame, None, 0.01), self.kernel1, iterations = 1)
    self.fgmask = cv2.dilate(self.fgmask, self.kernel2, iterations = 1)
    self.contours, self.hierarchy = cv2.findContours(self.fgmask.copy(), cv2.RETR_EXTERNAL,
                                                     cv2.CHAIN_APPROX_NONE)
    try: self.hierarchy = self.hierarchy[0]
    except: self.hierarchy = []

if __name__ == "__main__":
  cs = CameraSensor(disp = False)
  cs.run()
