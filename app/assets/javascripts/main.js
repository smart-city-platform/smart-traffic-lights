//main application caller
//------------------------
function main() {

    //var slider = new TrafficSlider();
    var mapElement  = $('#map')[0];
    var mapLocation = {lat: -23.558147, lng: -46.660198};
    var mapZoom = 15;
    var mapView = new StreetMap(mapLocation, mapElement, mapZoom);
    
    mapView.drawTrafficLights();
    mapView.drawStreetSections();
    
    var control = new Control(mapView);
    control.toggleVisibility($('#chk_lights'), mapView.trafficLights);
    control.toggleVisibility($('#chk_data'), mapView.streetSections);
}

//trigger main() when GMaps code is loaded
google.maps.event.addDomListener(window, 'load', main);
  

