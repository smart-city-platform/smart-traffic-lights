class CreateActuatorSensors < ActiveRecord::Migration
  def change
    create_table :actuator_sensors do |t|
      t.integer :actuator_id
      t.integer :sensor_id
      t.boolean :direct_influence

      t.timestamps null: false
    end
  end
end
