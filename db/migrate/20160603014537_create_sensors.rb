class CreateSensors < ActiveRecord::Migration
  def change
    create_table :sensors do |t|
      t.integer :street_section_id
      t.float :latitude
      t.float :longitude
      t.string :resource
      t.float :speed
      t.float :density

      t.timestamps null: false
    end
  end
end
